import { Component, ViewChild, OnInit } from '@angular/core';
import { MenuController, Nav, Platform, ToastController, Events, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '../providers/user';
import { Storage } from '@ionic/storage';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';



@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'HomeApp';
  user_profile: any;
  viewMenu: boolean = true;
  pages: Array<{ title: string, component: any, icon: string, role: number }>;

  constructor(
    public USER: User,
    public menuCtrl: MenuController,
    public events: Events,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private sanitizer: DomSanitizer,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    private fb: Facebook,
    private googlePlus: GooglePlus
  ) {
    this.user_profile = [];
    this.initializeApp();
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Servicios Trabajador', component: 'ServicesUser', icon: 'briefcase', role: 2 },
      { title: 'Servicios Cliente', component: 'ServicesClient', icon: 'briefcase', role: 3 }
    ];
  }

  ngOnInit() {
    this.USER.getProfileUser().subscribe(res => {
      if (res != null) {
        this.user_profile = res;
      }
      else {
        this.user_profile.idRole = 0
      }
    })
    // this.USER.initialiceProfileUser();
    // this.user_profile = this.USER.user_profile;

    this.events.subscribe('username:changed', username => {
      console.log("evento");
      if (username !== undefined && username !== "") {
        this.user_profile = username;
        let toast = this.toastCtrl.create({
          message: `Bienvenido de nuevo ${this.user_profile.name}`,
          duration: 3000,
          position: 'bottom',
          showCloseButton: true,
          closeButtonText: 'Aceptar'
        });
        toast.present();
      }
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.statusBar.styleDefault();
      this.statusBar.styleBlackTranslucent();
      // set status bar to white
      // this.statusBar.backgroundColorByHexString('#01579b');
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  closeSesion() {

    // clear storage
    this.storage.clear();
    // this.nav.popToRoot();
    // hidden menu botton
    this.menuCtrl.enable(false);
    // close facebook sesion
    this.fb.getLoginStatus().then(response => {
      if (response.status != "connected") {
        this.fb.logout();
      }
    }).catch(res => {
      console.log(res);
    });
    // close google sesion
    this.googlePlus.trySilentLogin({}).then(res => {
      this.googlePlus.logout()
    }).catch(res => {
      console.log(res);
    })
    // loading on close sesion
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: 'Loading Please Wait...'
    });
    loading.present();
    setTimeout(() => {
      this.nav.setRoot('Login');
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }
}
