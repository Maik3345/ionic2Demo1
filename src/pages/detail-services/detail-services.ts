import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Slides } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
/**
 * Generated class for the DetailServices page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-services',
  templateUrl: 'detail-services.html',
})

export class DetailServices implements OnInit {
  @ViewChild(Slides) slides: Slides;
  public infoservices: any;
  constructor(public navCtrl: NavController,
    private photoViewer: PhotoViewer,
    private callNumber: CallNumber,
    public navParams: NavParams) {
    this.infoservices = navParams.get("info");

    console.log(this.infoservices);
  }

  ngOnInit() {
    // adjusts the pages of the slide
    this.slides.autoHeight = true;
    // this.slides.parallax = true;
    this.slides.autoplay = true;
    this.slides.resize();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailServices');
  }

  viewImage(image) {
    this.photoViewer.show(image);
  }

  callUser(number) {
    this.callNumber.callNumber(number, false)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }


}
