import { Component, ChangeDetectorRef, trigger, state, style, animate, transition, group } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, FabContainer, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { ApiServices } from "../../providers/services-user";
import { User } from "../../providers/user";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/**
 * Generated class for the ServicesClient page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-services-client',
  templateUrl: 'services-client.html',
  animations: [
    trigger('shrinkOut', [
      state('in', style({ opacity: 1, transform: 'translateX(0)' })),
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 10 ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})
export class ServicesClient {

  public services_data: any;
  public showToolbar: boolean = false;
  public titlepage: string;
  public state: "active";

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams,
    public apiServices: ApiServices,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController,
    public ref: ChangeDetectorRef,
    public USER: User
  ) {
    this.menuCtrl.enable(true);
    this.statusBar.backgroundColorByHexString('#33000000');
    this.services_data = [];
    this.titlepage = "Servicios Cliente";


  }

  getServicesClient() {
    let loading = this.loadingCtrl.create({
      content: 'Consultando servicios...'
    });
    loading.present();
    this.services_data = [];

    this.USER.getProfileUser().subscribe(res => {
      this.apiServices.getServicesClient(res.id).subscribe(data => {
        this.services_data = data;
        loading.dismiss();
      });
    })
  }
  ngOnInit() {
    this.getServicesClient();
  }


  closeFab(fab?: FabContainer) {
    if (fab !== undefined) {
      fab.close();
    }
  }
  onScroll($event: any) {
    let scrollTop = $event.scrollTop;
    this.showToolbar = scrollTop >= 50;
    this.ref.detectChanges();
  }

  viewDetail(index) {
    this.navCtrl.push('DetailServices', { info: this.services_data[index] });
  }

}
