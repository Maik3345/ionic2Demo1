import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicesClient } from './services-client';

@NgModule({
  declarations: [
    ServicesClient,
  ],
  imports: [
    IonicPageModule.forChild(ServicesClient),
  ],
  exports: [
    ServicesClient
  ]
})
export class ServicesClientModule {}
