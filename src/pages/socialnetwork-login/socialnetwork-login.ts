import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { LoginAPI } from '../../providers/login';

/**
 * Generated class for the SocialnetworkLogin page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-socialnetwork-login',
  templateUrl: 'socialnetwork-login.html',
})
export class SocialnetworkLogin {

  public urlnetwork_login: string = "http://appservicios.ingeneo.co/api/loginNetworkSocial";
  public user_profile: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private fb: Facebook,
    private googlePlus: GooglePlus,
    public loginApi: LoginAPI,
    private toastCtrl: ToastController,
    public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SocialnetworkLogin');
  }

  closeModal() {
    let data = {
      view: 'Cancel sign in',
      user: null
    }
    this.viewCtrl.dismiss(data);
  }
  // login con google
  loginGoogle() {
    this.googlePlus.login({})
      .then((res) => {
        console.log(res);
        let data = {
          idNetworkSocial: res.userId
        }
        this.loginApi.login(this.urlnetwork_login, data).subscribe(response => {
          if (!response.err) {
            this.user_profile = response;
            let data = {
              view: 'ContinueLogin',
              user: this.user_profile
            }
            this.viewCtrl.dismiss(data);
          }
          else {
            let error = "Se ha presentado un error al iniciar sesión con google";
            this.loginErrorCallback(error);
          }
        });
      })
      .catch(response => {
        if (response == "User cancelled.") {
          response = "Ingreso cancelado.";
        }
        this.loginErrorCallback(response);
      });
  }


  // login con facebook
  loginFacebook() {
    this.fb.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        let data = {
          idNetworkSocial: res.authResponse.userID
        }
        this.loginApi.login(this.urlnetwork_login, data).subscribe(response => {
          if (!response.err) {
            this.user_profile = response;

            let data = {
              view: 'ContinueLogin',
              user: this.user_profile
            }
            this.viewCtrl.dismiss(data);
          }
          else {
            let error = "Se ha presentado un error al iniciar sesión con facebook";
            this.loginErrorCallback(error);
          }
        });
      })
      .catch(response => {
        if (response == "User cancelled.") {
          response = "Ingreso cancelado.";
        }
        this.loginErrorCallback(response);
      });
  }

  // callback de error para el proceso de login
  loginErrorCallback(error) {
    let toast = this.toastCtrl.create({
      message: error,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'Aceptar'
    });
    toast.present();
    console.log("error" + error);
  }

}
