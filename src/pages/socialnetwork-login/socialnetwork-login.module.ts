import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SocialnetworkLogin } from './socialnetwork-login';

@NgModule({
  declarations: [
    SocialnetworkLogin,
  ],
  imports: [
    IonicPageModule.forChild(SocialnetworkLogin),
  ],
  exports: [
    SocialnetworkLogin
  ]
})
export class SocialnetworkLoginModule {}
