import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../../providers/user';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the RecoveryPassword page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-recovery-password',
  templateUrl: 'recovery-password.html',
})
export class RecoveryPassword {
  private recovery_form: FormGroup;
  private url_recovery: string = "http://appservicios.ingeneo.co/api/user/recoverPassword";
  public email: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public USER: User,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    public statusBar: StatusBar,
    public viewCtrl: ViewController) {

    this.recovery_form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecoveryPassword');
    this.statusBar.backgroundColorByHexString('#33000000');
  }

  closeModal() {
    let data = {
      view: 'Cancel sign in',
      user: null
    }
    this.viewCtrl.dismiss(data);
  }

  recoveryPassword(formdata) {
    let loading = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    loading.present();
    let data =
      {
        email: formdata.email
      }
    this.USER.recoveryPassword(this.url_recovery, data).subscribe(res => {
      loading.dismiss();
      console.log(res);
      if (res.msg) {
        this.email = "";
        this.recoveryErrorCallback(res.msg);
        this.closeModal();
      }
      else if (res.err) {
        this.recoveryErrorCallback("Se ha presentado un error, verifica que el correo electronico sea correcto.");
      }
    })
  }

  // callback de error para el proceso de recuperación de contraseña
  recoveryErrorCallback(error) {
    let toast = this.toastCtrl.create({
      message: error,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'Aceptar'
    });
    toast.present();
  }


}
