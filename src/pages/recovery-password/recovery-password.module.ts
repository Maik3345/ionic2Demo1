import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecoveryPassword } from './recovery-password';

@NgModule({
  declarations: [
    RecoveryPassword,
  ],
  imports: [
    IonicPageModule.forChild(RecoveryPassword),
  ],
  exports: [
    RecoveryPassword
  ]
})
export class RecoveryPasswordModule {}
