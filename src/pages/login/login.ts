import { Component, ChangeDetectorRef } from '@angular/core';
import { MenuController, IonicPage, NavController, NavParams, LoadingController, ToastController, ModalController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http } from "@angular/http";
import { Md5 } from 'ts-md5/dist/md5';
import { LoginAPI } from '../../providers/login';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
  private login_form: FormGroup;

  public enterPassword: boolean = false;
  public email: string;
  public password: string;
  public name: string;
  public lastname: string;
  public showToolbar: boolean = false;
  public title: string = "Sign in";


  public url_login: string = "http://appservicios.ingeneo.co/api/login";
  public url_imageprofile: string = "http://appservicios.ingeneo.co/api/profile/getImageProfileUser";
  public imageuser: string;
  public user_profile: any;

  constructor(public menuCtrl: MenuController,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public statusBar: StatusBar,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public http: Http,
    public loginApi: LoginAPI,
    public ref: ChangeDetectorRef,
    public modalCtrl: ModalController
  ) {

    // creo los campos del formulario
    this.menuCtrl.enable(false);
    this.login_form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.minLength(3), Validators.required])]
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
    this.statusBar.backgroundColorByHexString('#33000000');
  }


  // Modal for open the view for select login social to login
  loginSocialNetwork() {
    let loginSocial = this.modalCtrl.create('SocialnetworkLogin');
    loginSocial.onDidDismiss(data => {
      if (data.view != "Cancel sign in") {
        this.navCtrl.push(data.view, {
          user_data: data.user
        });
      }
    });
    loginSocial.present();
  }

  // Modal to open the view for recovery the password
  recoveryPassword() {
    let loginSocial = this.modalCtrl.create('RecoveryPassword');
    loginSocial.onDidDismiss(data => {
      if (data.view != "Cancel sign in") {
        this.navCtrl.push(data.view, {
          user_data: data.user
        });
      }
    });
    loginSocial.present();
  }

  // function for close the view on user enter the password
  closeEnterPassword() {
    this.enterPassword = false;
    this.title = "Sign in";
  };

  getImageUser(formdata) {
    let loading = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    loading.present();
    let data = {
      email: formdata.email
    }
    this.loginApi.getImageUser(this.url_imageprofile, data).subscribe(response => {
      if (response.msg == "User not found.") {
        this.loginErrorCallback("El usuario no ha sido encontrado en el sistema");
      }
      else {
        this.imageuser = response.image;
        this.name = response.name;
        this.lastname = response.lastname;
        this.title = this.name + " " + this.lastname;
      }
      loading.dismiss();
    });
  }

  viewEnterPassword(formdata) {
    this.enterPassword = !this.enterPassword;
    this.getImageUser(formdata);
  }

  // funcion para iniciar sesión con la api de login de la app
  loginApp(formdata) {
    let data = {
      email: formdata.email,
      password: Md5.hashStr(formdata.password)
    }

    this.loginApi.login(this.url_login, data).subscribe(response => {
      if (!response.err) {
        this.user_profile = response;
        this.navCtrl.push('ContinueLogin', {
          user_data: this.user_profile
        });
      }
      else {
        this.loginErrorCallback(response.err);
      }
    }, error => {
      this.loginErrorCallback(error);
    })
  }

  // callback de error para el proceso de login
  loginErrorCallback(error) {
    let toast = this.toastCtrl.create({
      message: error,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'Aceptar'
    });
    toast.present();
  }

  onScroll($event: any) {
    let scrollTop = $event.scrollTop;
    this.showToolbar = scrollTop >= 30;
    // if (scrollTop <= 80) {
    //   this.poscroll = "-" + scrollTop + "px";
    // }
    // else {
    //   this.poscroll = "-" + 80 + "px";
    // }
    // [style.top]="poscroll"
    this.ref.detectChanges();
  }

}



