import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContinueLogin } from './continue-login';

@NgModule({
  declarations: [
    ContinueLogin,
  ],
  imports: [
    IonicPageModule.forChild(ContinueLogin),
  ],
  exports: [
    ContinueLogin
  ]
})
export class ContinueLoginModule {}
