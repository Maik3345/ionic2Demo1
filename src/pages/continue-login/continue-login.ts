import { Component } from '@angular/core';
import { MenuController, IonicPage, NavController, NavParams, Events, App, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the ContinueLogin page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-continue-login',
  templateUrl: 'continue-login.html',
})
export class ContinueLogin {
  public user_profile: any = {};


  constructor(public appCtrl: App,
    public menuCtrl: MenuController,
    public events: Events,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage) {

    this.statusBar.styleBlackTranslucent();
    this.user_profile = navParams.get('user_data');
    console.log("datos pasados");
    console.dir(this.user_profile);
    // save data of user in local storage
    this.storage.set('data_user', this.user_profile);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContinueLogin');

  }

  continueView() {
    this.storage.get('data_user').then((val) => {
      this.user_profile = val;
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: 'Loading Please Wait...'
      });

      loading.present();

      if (this.user_profile.idRole == 2 || this.user_profile.idRole == 4) {
        // this.navCtrl.push(HomePage);
        this.events.publish('username:changed', this.user_profile);
        setTimeout(() => {
          this.navCtrl.setRoot('ServicesUser');
        }, 1000);
        setTimeout(() => {
          loading.dismiss();
        }, 1000);

      }
      else if (this.user_profile.idRole == 3) {
        this.events.publish('username:changed', this.user_profile);
        setTimeout(() => {
          this.navCtrl.setRoot('ServicesClient');
        }, 1000);
        setTimeout(() => {
          loading.dismiss();
        }, 1000);

      }
    })

  }


}


