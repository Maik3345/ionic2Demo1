import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeApp } from './home-app';

@NgModule({
  declarations: [
    HomeApp,
  ],
  imports: [
    IonicPageModule.forChild(HomeApp),
  ],
  exports: [
    HomeApp
  ]
})
export class HomeAppModule {}
