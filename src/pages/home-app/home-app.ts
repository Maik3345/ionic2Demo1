import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { User } from '../../providers/user';

/**
 * Generated class for the HomeApp page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-home-app',
  templateUrl: 'home-app.html',
})
export class HomeApp {

  viewContHomeApp: boolean = false;

  constructor(public menuCtrl: MenuController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController,
    public USER: User,
    private toastCtrl: ToastController
  ) {
    this.menuCtrl.enable(false);
    // setInterval(() => this.goToPrimaryView(), 5000);
    this.USER.getProfileUser().subscribe(profile => {
      if (profile != null) {
        this.menuCtrl.enable(true);
        // this.USER.initialiceProfileUser();
        if (profile.idRole == 2 || profile.idRole == 4) {
          this.navCtrl.setRoot('ServicesUser');
        }
        else if (profile.idRole == 3) {
          this.navCtrl.setRoot('ServicesClient');
        }
        let toast = this.toastCtrl.create({
          message: `Bienvenido de nuevo ${profile.name}`,
          duration: 3000,
          position: 'bottom',
          showCloseButton: true,
          closeButtonText: 'Aceptar'
        });
        toast.present();
      }
      else {
        this.viewContHomeApp = true;
      }
    });
  }


  ionViewDidLoad() {
    // this.statusBar.backgroundColorByHexString('#bdbdbd');
    console.log('ionViewDidLoad HomeApp');
  }

  goToPrimaryView() {

    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: 'Loading Please Wait...'
    });

    loading.present();

    setTimeout(() => {
      this.navCtrl.setRoot("Login");
    }, 1000);
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }

}
