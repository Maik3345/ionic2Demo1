import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, FabContainer, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { ApiServices } from "../../providers/services-user";

/**
 * Generated class for the ServicesUser page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-services-user',
  templateUrl: 'services-user.html',
})
export class ServicesUser {
  // public services_data: Array<any> = [];
  public services_data: any;
  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams,
    public apiServices: ApiServices,
    public statusBar: StatusBar,
    public loadingCtrl: LoadingController,
    public ref: ChangeDetectorRef
  ) {
    this.menuCtrl.enable(true);
    this.statusBar.backgroundColorByHexString('#33000000');
  }

  getServices() {
    let loading = this.loadingCtrl.create({
      content: 'Consultando servicios...'
    });
    loading.present();
    this.services_data = [];
    this.apiServices.getServices().subscribe(data => {
      this.services_data = data;
      loading.dismiss();

    });
  }
  ngOnInit() {
    this.getServices();
  }
  closeFab(fab?: FabContainer) {
    // console.log(fab)
    if (fab !== undefined) {
      fab.close();
    }
  }


}
