import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicesUser } from './services-user';

@NgModule({
  declarations: [
    ServicesUser,
  ],
  imports: [
    IonicPageModule.forChild(ServicesUser),
  ],
  exports: [
    ServicesUser
  ]
})
export class ServicesUserModule {}
