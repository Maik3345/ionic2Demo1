
import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

/*
  Generated class for the ApiServices provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LoginAPI {

    constructor(public http: Http,
        private fb: Facebook,
        private toastCtrl: ToastController,
        public loadingCtrl: LoadingController
    ) {
        console.log('Hello LoginAPI Provider');
    }

    login(url_login, data) {
        return this.http.post(url_login, data).map((res: any) => res.json())
    }
    getImageUser(url, data) {
        return this.http.post(url, data).map((res: any) => res.json())
    }
}

