import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable/';
import 'rxjs/add/observable/fromPromise';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

/*
  Generated class for the User provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class User {
  user_profile: any;

  constructor(
    public http: Http,
    public storage: Storage
  ) {
    console.log('Hello User Provider');
  }

  initialiceProfileUser() {
    this.storage.get('data_user').then((res) => {
      if (res != null) {
        this.user_profile = res;
      }
      else {
        this.user_profile.idRole = 0
      }
    });
  }

  getProfileUser() {
    return Observable.fromPromise(this.storage.get('data_user').then((res) => {
      return res;
    }));
  }

  recoveryPassword(url_login, data) {
    return this.http.post(url_login, data).map((res: any) => res.json())
  }

}
