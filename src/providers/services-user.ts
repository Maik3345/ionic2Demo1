import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ApiServices provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ApiServices {

  constructor(public http: Http) {
    console.log('Hello ApiServices Provider');
  }

  getServices() {
    let url = 'http://appservicios.ingeneo.co/api/jobApplication/getJobsByUser';
    let data = {
      id: 337
    }
    return this.http.post(url, data).map((res: any) => res.json())
  }
  getServicesClient(iduser) {
    let url = 'http://appservicios.ingeneo.co/api/jobApplication/getJobsByUser';
    let data = {
      id: iduser
    }
    return this.http.post(url, data).map((res: any) => res.json())
  }

}
